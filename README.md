# GitLab.com Monitoring with Count Terraform Module

## What is this?

This module provisions monitoring GCE instances.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.0.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_bootstrap"></a> [bootstrap](#module\_bootstrap) | ops.gitlab.net/gitlab-com/bootstrap/google | 5.1.6 |

## Resources

| Name | Type |
|------|------|
| [google_compute_address.external](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_address.static-ip-address](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_backend_service.iap](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_backend_service) | resource |
| [google_compute_disk.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.log_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk_resource_policy_attachment.data_disk_snapshot_policy_attachment](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk_resource_policy_attachment) | resource |
| [google_compute_firewall.monitoring_whitelist](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.public](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_health_check.http](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_health_check) | resource |
| [google_compute_health_check.tcp](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_health_check) | resource |
| [google_compute_instance.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance) | resource |
| [google_compute_instance_group.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance_group) | resource |
| [google_compute_region_backend_service.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_region_backend_service) | resource |
| [google_compute_resource_policy.data_disk_snapshot_policy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_resource_policy) | resource |
| [google_compute_subnetwork.subnetwork](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork) | resource |
| [google_compute_subnetwork.subnetwork](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_subnetwork) | data source |
| [google_compute_zones.available](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_zones) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_stopping_for_update"></a> [allow\_stopping\_for\_update](#input\_allow\_stopping\_for\_update) | Whether Terraform is allowed to stop the instance to update its properties | `bool` | `false` | no |
| <a name="input_assign_public_ip"></a> [assign\_public\_ip](#input\_assign\_public\_ip) | Instances without public IPs cannot access the public internet without NAT. Ensure that a Cloud NAT instance covers the subnetwork/region for this instance. | `bool` | `true` | no |
| <a name="input_backend_protocol"></a> [backend\_protocol](#input\_backend\_protocol) | n/a | `string` | `"HTTP"` | no |
| <a name="input_block_project_ssh_keys"></a> [block\_project\_ssh\_keys](#input\_block\_project\_ssh\_keys) | Whether to block project level SSH keys | `string` | `"TRUE"` | no |
| <a name="input_bootstrap_script"></a> [bootstrap\_script](#input\_bootstrap\_script) | User-provided bootstrap script to override the bootstrap module | `string` | `null` | no |
| <a name="input_chef_provision"></a> [chef\_provision](#input\_chef\_provision) | Configuration details for chef server | `map(string)` | n/a | yes |
| <a name="input_chef_run_list"></a> [chef\_run\_list](#input\_chef\_run\_list) | run\_list for the node in chef | `string` | n/a | yes |
| <a name="input_data_disk_size"></a> [data\_disk\_size](#input\_data\_disk\_size) | The size of the data disk | `number` | `20` | no |
| <a name="input_data_disk_snapshot"></a> [data\_disk\_snapshot](#input\_data\_disk\_snapshot) | Restore data disk from a snapshot | `string` | `""` | no |
| <a name="input_data_disk_snapshot_max_retention_days"></a> [data\_disk\_snapshot\_max\_retention\_days](#input\_data\_disk\_snapshot\_max\_retention\_days) | Number of days to retain snapshots, defaults to 2 weeks | `number` | `14` | no |
| <a name="input_data_disk_type"></a> [data\_disk\_type](#input\_data\_disk\_type) | The type of the data disk | `string` | `"pd-standard"` | no |
| <a name="input_dns_zone_name"></a> [dns\_zone\_name](#input\_dns\_zone\_name) | The GCP name of the DNS zone to use for this environment | `string` | n/a | yes |
| <a name="input_enable_oslogin"></a> [enable\_oslogin](#input\_enable\_oslogin) | Whether to enable OS Login GCP feature | `string` | `"FALSE"` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment name | `string` | n/a | yes |
| <a name="input_format_data_disk"></a> [format\_data\_disk](#input\_format\_data\_disk) | Force formatting of the persistent disk. | `string` | `"false"` | no |
| <a name="input_fw_whitelist_ports"></a> [fw\_whitelist\_ports](#input\_fw\_whitelist\_ports) | n/a | `list(string)` | `[]` | no |
| <a name="input_fw_whitelist_subnets"></a> [fw\_whitelist\_subnets](#input\_fw\_whitelist\_subnets) | n/a | `list(string)` | `[]` | no |
| <a name="input_health_check"></a> [health\_check](#input\_health\_check) | n/a | `string` | `"http"` | no |
| <a name="input_hours_between_data_disk_snapshots"></a> [hours\_between\_data\_disk\_snapshots](#input\_hours\_between\_data\_disk\_snapshots) | Setting this will enable data disk snapshots with staggered start times, 0 to disable | `number` | `0` | no |
| <a name="input_ip_cidr_range"></a> [ip\_cidr\_range](#input\_ip\_cidr\_range) | n/a | `string` | `"169.254.0.1/32"` | no |
| <a name="input_kernel_version"></a> [kernel\_version](#input\_kernel\_version) | n/a | `string` | `""` | no |
| <a name="input_labels"></a> [labels](#input\_labels) | Labels to add to each of the managed resources. | `map(string)` | `{}` | no |
| <a name="input_log_disk_size"></a> [log\_disk\_size](#input\_log\_disk\_size) | The size of the log disk | `number` | `50` | no |
| <a name="input_log_disk_type"></a> [log\_disk\_type](#input\_log\_disk\_type) | The type of the log disk | `string` | `"pd-standard"` | no |
| <a name="input_machine_type"></a> [machine\_type](#input\_machine\_type) | The machine size | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | The pet name | `string` | n/a | yes |
| <a name="input_node_count"></a> [node\_count](#input\_node\_count) | n/a | `number` | n/a | yes |
| <a name="input_oauth2_client_id"></a> [oauth2\_client\_id](#input\_oauth2\_client\_id) | n/a | `string` | `""` | no |
| <a name="input_oauth2_client_secret"></a> [oauth2\_client\_secret](#input\_oauth2\_client\_secret) | n/a | `string` | `""` | no |
| <a name="input_os_boot_image"></a> [os\_boot\_image](#input\_os\_boot\_image) | The OS image to boot | `string` | `"ubuntu-os-cloud/ubuntu-1804-lts"` | no |
| <a name="input_os_disk_size"></a> [os\_disk\_size](#input\_os\_disk\_size) | The OS disk size in GiB | `number` | `20` | no |
| <a name="input_os_disk_type"></a> [os\_disk\_type](#input\_os\_disk\_type) | The OS disk type | `string` | `"pd-standard"` | no |
| <a name="input_per_node_data_disk_snapshot_max_retention_days"></a> [per\_node\_data\_disk\_snapshot\_max\_retention\_days](#input\_per\_node\_data\_disk\_snapshot\_max\_retention\_days) | Override the number of days snapshots are retained for the data disk on select nodes. Map key s hould be the node number as an integer, starting at 1 | `map(number)` | `{}` | no |
| <a name="input_per_node_hours_between_data_disk_snapshots"></a> [per\_node\_hours\_between\_data\_disk\_snapshots](#input\_per\_node\_hours\_between\_data\_disk\_snapshots) | Override the number of hours between data disk snapshots (var.hours\_between\_data\_disk\_snapshots) for select nodes. Map key should be the node number as an integer, starting at 1 | `map(number)` | `{}` | no |
| <a name="input_persistent_disk_path"></a> [persistent\_disk\_path](#input\_persistent\_disk\_path) | default location for disk mount | `string` | `"/var/opt/gitlab"` | no |
| <a name="input_preemptible"></a> [preemptible](#input\_preemptible) | Use preemptible instances for this pet | `bool` | `false` | no |
| <a name="input_project"></a> [project](#input\_project) | The project name | `string` | n/a | yes |
| <a name="input_public_ports"></a> [public\_ports](#input\_public\_ports) | The list of ports that should be publicly reachable | `list(string)` | `[]` | no |
| <a name="input_region"></a> [region](#input\_region) | The target region | `string` | n/a | yes |
| <a name="input_regional_backend_service"></a> [regional\_backend\_service](#input\_regional\_backend\_service) | Create regional backend service for internal load balancing. Can be used in conjunction with external\_backend\_service | `bool` | `false` | no |
| <a name="input_service_account_email"></a> [service\_account\_email](#input\_service\_account\_email) | Service account emails under which the instance is running | `string` | n/a | yes |
| <a name="input_service_ports"></a> [service\_ports](#input\_service\_ports) | Named ports for the service running on the monitoring node | <pre>list(object({<br>    name              = string<br>    port              = number<br>    health_check_port = optional(number)<br>    health_check_path = optional(string)<br>    iap               = optional(bool, true)<br>  }))</pre> | `[]` | no |
| <a name="input_static_private_ip"></a> [static\_private\_ip](#input\_static\_private\_ip) | Create Google compute instances with static internal IP addresses | `bool` | `false` | no |
| <a name="input_static_private_ip_auto_assign"></a> [static\_private\_ip\_auto\_assign](#input\_static\_private\_ip\_auto\_assign) | Let the static internal IP addresses be assigned automatically instead of assigning them explicitly | `bool` | `false` | no |
| <a name="input_subnetwork_name"></a> [subnetwork\_name](#input\_subnetwork\_name) | subnetwork name for the instances | `string` | `""` | no |
| <a name="input_teardown_script"></a> [teardown\_script](#input\_teardown\_script) | User-provided teardown script to override the bootstrap module | `string` | `null` | no |
| <a name="input_tier"></a> [tier](#input\_tier) | The tier for this service | `string` | n/a | yes |
| <a name="input_use_external_ip"></a> [use\_external\_ip](#input\_use\_external\_ip) | n/a | `bool` | `false` | no |
| <a name="input_use_new_node_name"></a> [use\_new\_node\_name](#input\_use\_new\_node\_name) | n/a | `bool` | `false` | no |
| <a name="input_vpc"></a> [vpc](#input\_vpc) | n/a | `string` | `""` | no |
| <a name="input_zone"></a> [zone](#input\_zone) | n/a | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_google_compute_address_static_ips"></a> [google\_compute\_address\_static\_ips](#output\_google\_compute\_address\_static\_ips) | n/a |
| <a name="output_google_compute_backend_service_self_link"></a> [google\_compute\_backend\_service\_self\_link](#output\_google\_compute\_backend\_service\_self\_link) | this is idiomatic for how to have outputs that may have a count of zero https://www.terraform.io/upgrade-guides/0-11.html#referencing-attributes-from-resources-with-count-0 |
| <a name="output_google_compute_region_backend_service_self_link"></a> [google\_compute\_region\_backend\_service\_self\_link](#output\_google\_compute\_region\_backend\_service\_self\_link) | n/a |
| <a name="output_http_health_check_self_link"></a> [http\_health\_check\_self\_link](#output\_http\_health\_check\_self\_link) | n/a |
| <a name="output_instance_groups_self_link"></a> [instance\_groups\_self\_link](#output\_instance\_groups\_self\_link) | n/a |
| <a name="output_instances"></a> [instances](#output\_instances) | n/a |
| <a name="output_instances_self_link"></a> [instances\_self\_link](#output\_instances\_self\_link) | n/a |
| <a name="output_tcp_health_check_self_link"></a> [tcp\_health\_check\_self\_link](#output\_tcp\_health\_check\_self\_link) | n/a |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
