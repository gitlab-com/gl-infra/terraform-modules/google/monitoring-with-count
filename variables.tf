variable "fw_whitelist_subnets" {
  type    = list(string)
  default = []
}

variable "fw_whitelist_ports" {
  type    = list(string)
  default = []
}

variable "data_disk_snapshot" {
  type        = string
  description = "Restore data disk from a snapshot"

  # An empty string will not use a snapshot for the data disk
  default = ""
}

variable "hours_between_data_disk_snapshots" {
  type        = number
  description = "Setting this will enable data disk snapshots with staggered start times, 0 to disable"
  default     = 0
}

variable "data_disk_snapshot_max_retention_days" {
  type        = number
  description = "Number of days to retain snapshots, defaults to 2 weeks"
  default     = 14
}

variable "per_node_hours_between_data_disk_snapshots" {
  type        = map(number)
  description = "Override the number of hours between data disk snapshots (var.hours_between_data_disk_snapshots) for select nodes. Map key should be the node number as an integer, starting at 1"
  default     = {}
}

variable "per_node_data_disk_snapshot_max_retention_days" {
  type        = map(number)
  description = "Override the number of days snapshots are retained for the data disk on select nodes. Map key s hould be the node number as an integer, starting at 1"
  default     = {}
}

variable "public_ports" {
  type        = list(string)
  description = "The list of ports that should be publicly reachable"
  default     = []
}

variable "use_external_ip" {
  type    = bool
  default = false
}

variable "static_private_ip" {
  type        = bool
  default     = false
  description = "Create Google compute instances with static internal IP addresses"
}

variable "static_private_ip_auto_assign" {
  type        = bool
  default     = false
  description = "Let the static internal IP addresses be assigned automatically instead of assigning them explicitly"
}

variable "regional_backend_service" {
  type        = bool
  default     = false
  description = "Create regional backend service for internal load balancing. Can be used in conjunction with external_backend_service"
}

variable "kernel_version" {
  type    = string
  default = ""
}

variable "use_new_node_name" {
  type    = bool
  default = false
}

variable "backend_protocol" {
  type    = string
  default = "HTTP"
}

variable "allow_stopping_for_update" {
  type        = bool
  description = "Whether Terraform is allowed to stop the instance to update its properties"
  default     = false
}

variable "vpc" {
  type    = string
  default = ""
}

variable "ip_cidr_range" {
  type    = string
  default = "169.254.0.1/32"
}

variable "node_count" {
  type = number
}

variable "health_check" {
  type    = string
  default = "http"

  validation {
    condition     = contains(["http", "tcp"], var.health_check)
    error_message = "The health_check value must be \"http\" or \"tcp\"."
  }
}

variable "oauth2_client_id" {
  type    = string
  default = ""
}

variable "oauth2_client_secret" {
  type    = string
  default = ""
}

# Structure
# [
#   {
#     name: "port-name",
#     port: 8080,
#     health_check_port: <optional>,
#     health_check_path: <mandatory if using http health check>,
#     iap: <true|false, defaults to true>,
#   }
# ]
variable "service_ports" {
  type = list(object({
    name              = string
    port              = number
    health_check_port = optional(number)
    health_check_path = optional(string)
    iap               = optional(bool, true)
  }))
  default     = []
  description = "Named ports for the service running on the monitoring node"
}

variable "subnetwork_name" {
  type        = string
  default     = ""
  description = "subnetwork name for the instances"
}

variable "block_project_ssh_keys" {
  type        = string
  description = "Whether to block project level SSH keys"
  default     = "TRUE"
}

variable "enable_oslogin" {
  type        = string
  description = "Whether to enable OS Login GCP feature"

  # Note: setting this to TRUE breaks chef!
  # https://gitlab.com/gitlab-com/gitlab-com-infrastructure/merge_requests/297#note_66690562
  default = "FALSE"
}

variable "persistent_disk_path" {
  type        = string
  description = "default location for disk mount"
  default     = "/var/opt/gitlab"
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"
}

variable "chef_run_list" {
  type        = string
  description = "run_list for the node in chef"
}

variable "data_disk_size" {
  type        = number
  description = "The size of the data disk"
  default     = 20
}

variable "data_disk_type" {
  type        = string
  description = "The type of the data disk"
  default     = "pd-standard"
}

variable "dns_zone_name" {
  type        = string
  description = "The GCP name of the DNS zone to use for this environment"
}

variable "environment" {
  type        = string
  description = "The environment name"
}

variable "log_disk_size" {
  type        = number
  description = "The size of the log disk"
  default     = 50
}

variable "log_disk_type" {
  type        = string
  description = "The type of the log disk"
  default     = "pd-standard"
}

variable "format_data_disk" {
  type        = string
  description = "Force formatting of the persistent disk."
  default     = "false"
}

variable "machine_type" {
  type        = string
  description = "The machine size"
}

variable "name" {
  type        = string
  description = "The pet name"
}

variable "os_boot_image" {
  type        = string
  description = "The OS image to boot"
  default     = "ubuntu-os-cloud/ubuntu-1804-lts"
}

variable "os_disk_size" {
  type        = number
  description = "The OS disk size in GiB"
  default     = 20
}

variable "os_disk_type" {
  type        = string
  description = "The OS disk type"
  default     = "pd-standard"
}

variable "preemptible" {
  type        = bool
  description = "Use preemptible instances for this pet"
  default     = false
}

variable "project" {
  type        = string
  description = "The project name"
}

variable "region" {
  type        = string
  description = "The target region"
}

variable "service_account_email" {
  type        = string
  description = "Service account emails under which the instance is running"
}

variable "bootstrap_script" {
  type        = string
  description = "User-provided bootstrap script to override the bootstrap module"
  default     = null
}

variable "teardown_script" {
  type        = string
  description = "User-provided teardown script to override the bootstrap module"
  default     = null
}

variable "tier" {
  type        = string
  description = "The tier for this service"
}

variable "zone" {
  type    = string
  default = ""
}

# Instances without public IPs cannot access the public internet without NAT.
# Ensure that a Cloud NAT instance covers the subnetwork/region for this
# instance.
variable "assign_public_ip" {
  type    = bool
  default = true
}

# Resource labels
variable "labels" {
  type        = map(string)
  description = "Labels to add to each of the managed resources."
  default     = {}
}
