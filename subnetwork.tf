resource "google_compute_subnetwork" "subnetwork" {
  count = var.subnetwork_name == "" ? 1 : 0

  name                     = format("%v-%v", var.name, var.environment)
  network                  = var.vpc
  ip_cidr_range            = var.ip_cidr_range
  private_ip_google_access = true
}
