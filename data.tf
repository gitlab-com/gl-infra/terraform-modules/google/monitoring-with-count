data "google_compute_zones" "available" {
  region = var.region
  status = "UP"
}

data "google_compute_subnetwork" "subnetwork" {
  count  = var.subnetwork_name == "" ? 0 : 1
  name   = var.subnetwork_name
  region = var.region
}
