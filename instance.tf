module "bootstrap" {
  source  = "ops.gitlab.net/gitlab-com/bootstrap/google"
  version = "5.1.6"
}

resource "google_compute_address" "external" {
  count = var.use_external_ip ? var.node_count : 0

  name = format(
    "%v-%02d-%v-%v-static-ip",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  address_type = "EXTERNAL"
  region       = var.region
}

resource "google_compute_address" "static-ip-address" {
  count = var.static_private_ip ? var.node_count : 0
  name = format(
    "%v-%02d-%v-%v-static-ip",
    var.name,
    count.index + 1 + 100,
    var.tier,
    var.environment,
  )
  address_type = "INTERNAL"

  address    = var.static_private_ip_auto_assign ? null : cidrhost(var.ip_cidr_range, count.index + 1 + 100)
  subnetwork = var.subnetwork_name == "" ? google_compute_subnetwork.subnetwork[0].self_link : data.google_compute_subnetwork.subnetwork[0].self_link
  region     = var.region
}

# This works around terraform limitation of using resources as arrays that
# don't necessary exist. https://github.com/hashicorp/terraform/issues/9858#issuecomment-386431631

locals {
  external_ips = concat(google_compute_address.external[*].address, [""])
}

resource "google_compute_region_backend_service" "default" {
  count = var.regional_backend_service ? length(var.service_ports) : 0

  # Avoid changing name for existing backend services, that predate count ever
  # being > 1.
  name                  = format("%v-%v-regional%v", var.environment, var.name, count.index > 0 ? format("-%02d", count.index) : "")
  protocol              = var.backend_protocol
  load_balancing_scheme = var.backend_protocol == "HTTP" || var.backend_protocol == "HTTPS" ? "INTERNAL_MANAGED" : "INTERNAL"
  port_name             = var.service_ports[count.index].name

  backend {
    balancing_mode = var.backend_protocol == "HTTP" || var.backend_protocol == "HTTPS" ? "UTILIZATION" : "CONNECTION"
    group          = google_compute_instance_group.default[0].self_link
  }

  backend {
    balancing_mode = var.backend_protocol == "HTTP" || var.backend_protocol == "HTTPS" ? "UTILIZATION" : "CONNECTION"
    group          = google_compute_instance_group.default[1].self_link
  }

  backend {
    balancing_mode = var.backend_protocol == "HTTP" || var.backend_protocol == "HTTPS" ? "UTILIZATION" : "CONNECTION"
    group          = google_compute_instance_group.default[2].self_link
  }

  health_checks = [var.health_check == "http" ? google_compute_health_check.http[count.index].self_link : google_compute_health_check.tcp[count.index].self_link]
}

resource "google_compute_backend_service" "iap" {
  count = length(var.service_ports)

  # Avoid changing name for existing backend services, that predate count ever
  # being > 1.
  name      = format("%v-%v%v", var.environment, var.name, count.index > 0 ? format("-%02d", count.index) : "")
  protocol  = var.backend_protocol
  port_name = var.service_ports[count.index].name

  backend {
    balancing_mode = var.backend_protocol == "HTTP" || var.backend_protocol == "HTTPS" ? "UTILIZATION" : "CONNECTION"
    group          = google_compute_instance_group.default[0].self_link
  }

  backend {
    balancing_mode = var.backend_protocol == "HTTP" || var.backend_protocol == "HTTPS" ? "UTILIZATION" : "CONNECTION"
    group          = google_compute_instance_group.default[1].self_link
  }

  backend {
    balancing_mode = var.backend_protocol == "HTTP" || var.backend_protocol == "HTTPS" ? "UTILIZATION" : "CONNECTION"
    group          = google_compute_instance_group.default[2].self_link
  }

  health_checks = [var.health_check == "http" ? google_compute_health_check.http[count.index].self_link : google_compute_health_check.tcp[count.index].self_link]

  dynamic "iap" {
    for_each = range(var.service_ports[count.index].iap ? 1 : 0)

    content {
      oauth2_client_secret = var.oauth2_client_secret
      oauth2_client_id     = var.oauth2_client_id
    }
  }
}

resource "google_compute_health_check" "tcp" {
  count = var.health_check == "tcp" ? length(var.service_ports) : 0

  # GCP won't allow us to replace health checks on existing backend services, so
  # we must not change the names of existing health checks - i.e. those with
  # index 0, that were created before this resource had count > 1.
  name = format("%v-%v-tcp%v", var.environment, var.name, count.index > 0 ? format("-%02d", count.index) : "")

  tcp_health_check {
    port = var.service_ports[count.index].health_check_port != null ? var.service_ports[count.index].health_check_port : var.service_ports[count.index].port
  }
}

resource "google_compute_health_check" "http" {
  count = var.health_check == "http" ? length(var.service_ports) : 0

  # GCP won't allow us to replace health checks on existing backend services, so
  # we must not change the names of existing health checks - i.e. those with
  # index 0, that were created before this resource had count > 1.
  name = format("%v-%v-http%v", var.environment, var.name, count.index > 0 ? format("-%02d", count.index) : "")

  http_health_check {
    port         = var.service_ports[count.index].health_check_port != null ? var.service_ports[count.index].health_check_port : var.service_ports[count.index].port
    request_path = var.service_ports[count.index].health_check_path
  }
}

# Add one instance group per zone
# and only select the appropriate instances
# for each one

resource "google_compute_instance_group" "default" {
  count = length(data.google_compute_zones.available.names)

  name = format(
    "%v-%v-%v",
    var.environment,
    var.name,
    data.google_compute_zones.available.names[count.index],
  )
  description = "Instance group for monitoring VM."
  zone        = data.google_compute_zones.available.names[count.index]

  dynamic "named_port" {
    for_each = var.service_ports

    content {
      name = named_port.value.name
      port = named_port.value.port
    }
  }

  named_port {
    name = "http"
    port = 80
  }

  named_port {
    name = "https"
    port = 443
  }

  # This filters the full set of instances to only ones for the appropriate zone.
  instances = matchkeys(
    google_compute_instance.default[*].self_link,
    google_compute_instance.default[*].zone,
    [data.google_compute_zones.available.names[count.index]],
  )
}

resource "google_compute_disk" "default" {
  count = var.node_count

  project = var.project
  name = format(
    "%v-%02d-%v-%v-data",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  zone     = var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)
  size     = var.data_disk_size
  type     = var.data_disk_type
  snapshot = var.data_disk_snapshot

  labels = merge(var.labels, {
    environment  = var.environment
    pet_name     = var.name
    do_snapshots = "true"
  })

  lifecycle {
    ignore_changes = [snapshot]
  }
}

resource "google_compute_disk_resource_policy_attachment" "data_disk_snapshot_policy_attachment" {
  count = var.hours_between_data_disk_snapshots > 0 ? var.node_count : 0

  name = google_compute_resource_policy.data_disk_snapshot_policy[count.index].name
  disk = google_compute_disk.default[count.index].name
  zone = google_compute_disk.default[count.index].zone
  lifecycle {
    # This lifecycle rule is necessary because you cannot modify policies when
    # they are in use
    replace_triggered_by = [
      google_compute_resource_policy.data_disk_snapshot_policy[count.index].id
    ]
  }
}

resource "google_compute_disk" "log_disk" {
  count = var.node_count

  project = var.project
  name = format(
    "%v-%02d-%v-%v-log",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  zone = var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)
  size = var.log_disk_size
  type = var.log_disk_type

  labels = merge(var.labels, {
    environment = var.environment
  })
}

resource "google_compute_instance" "default" {
  count = var.node_count

  name = format(
    "%v-%02d-%v-%v",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  machine_type = var.machine_type

  metadata = {
    "CHEF_URL"         = var.chef_provision["server_url"]
    "CHEF_VERSION"     = var.chef_provision["version"]
    "CHEF_ENVIRONMENT" = var.environment
    "CHEF_NODE_NAME" = var.use_new_node_name ? format(
      "%v-%02d-%v-%v.c.%v.internal",
      var.name,
      count.index + 1,
      var.tier,
      var.environment,
      var.project,
      ) : format(
      "%v-%02d.%v.%v.%v",
      var.name,
      count.index + 1,
      var.tier,
      var.environment,
      var.dns_zone_name,
    )
    "GL_KERNEL_VERSION"       = var.kernel_version
    "CHEF_RUN_LIST"           = var.chef_run_list
    "CHEF_DNS_ZONE_NAME"      = var.dns_zone_name
    "CHEF_PROJECT"            = var.project
    "CHEF_BOOTSTRAP_BUCKET"   = var.chef_provision["bootstrap_bucket"]
    "CHEF_BOOTSTRAP_KEYRING"  = var.chef_provision["bootstrap_keyring"]
    "CHEF_BOOTSTRAP_KEY"      = var.chef_provision["bootstrap_key"]
    "GL_PERSISTENT_DISK_PATH" = var.persistent_disk_path
    "GL_FORMAT_DATA_DISK"     = var.format_data_disk
    "block-project-ssh-keys"  = var.block_project_ssh_keys
    "enable-oslogin"          = var.enable_oslogin
    "shutdown-script"         = var.teardown_script != null ? var.teardown_script : module.bootstrap.teardown
    "startup-script"          = var.bootstrap_script != null ? var.bootstrap_script : module.bootstrap.bootstrap
  }

  allow_stopping_for_update = var.allow_stopping_for_update
  project                   = var.project

  zone = var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email = var.service_account_email

    // all the defaults plus cloudkms to access kms
    scopes = [
      "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/pubsub",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/cloudkms",
      "https://www.googleapis.com/auth/compute.readonly",
    ]
  }

  scheduling {
    preemptible = var.preemptible
  }

  boot_disk {
    auto_delete = true

    initialize_params {
      image  = var.os_boot_image
      size   = var.os_disk_size
      type   = var.os_disk_type
      labels = var.labels
    }
  }

  attached_disk {
    source = google_compute_disk.default[count.index].self_link
  }

  attached_disk {
    source      = google_compute_disk.log_disk[count.index].self_link
    device_name = "log"
  }

  network_interface {
    subnetwork = var.subnetwork_name != "" ? var.subnetwork_name : join("", google_compute_subnetwork.subnetwork[*].name)
    network_ip = var.static_private_ip ? google_compute_address.static-ip-address[count.index].address : ""

    dynamic "access_config" {
      for_each = var.assign_public_ip ? [0] : []
      content {
        nat_ip = var.use_external_ip ? element(local.external_ips, count.index) : ""
      }
    }
  }

  labels = merge(var.labels, {
    environment = var.environment
    pet_name    = var.name
  })

  tags = [
    var.name,
    var.environment,
  ]

  lifecycle {
    ignore_changes = [min_cpu_platform, boot_disk, machine_type]
  }
}
