resource "google_compute_firewall" "public" {
  count = length(var.public_ports) > 0 ? 1 : 0

  name    = format("%v-%v", var.name, var.environment)
  network = var.vpc

  allow {
    protocol = "tcp"
    ports    = var.public_ports
  }

  source_ranges = ["0.0.0.0/0"]

  target_tags = [var.name]
}

resource "google_compute_firewall" "monitoring_whitelist" {
  count = length(var.fw_whitelist_subnets) > 0 ? 1 : 0

  name    = format("%v-%v-monitoring-whitelist", var.name, var.environment)
  network = var.vpc

  allow {
    protocol = "tcp"
    ports    = var.fw_whitelist_ports
  }

  source_ranges = var.fw_whitelist_subnets

  target_tags = [var.name]
}
