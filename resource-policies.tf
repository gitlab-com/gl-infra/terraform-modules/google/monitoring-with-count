resource "google_compute_resource_policy" "data_disk_snapshot_policy" {
  count = var.hours_between_data_disk_snapshots > 0 ? var.node_count : 0
  name = format(
    "%v-%02d-data-disk-snapshot-policy",
    var.name,
    count.index + 1,
  )

  snapshot_schedule_policy {
    schedule {
      hourly_schedule {
        hours_in_cycle = lookup(var.per_node_hours_between_data_disk_snapshots, count.index + 1, var.hours_between_data_disk_snapshots)
        # Time within the window to start the operations.
        # It must be in an hourly format "HH:MM", where HH : [00-23] and MM : [00] GMT. eg: 21:00
        start_time = format("%02d:00", count.index % 24)
      }
    }

    retention_policy {
      max_retention_days    = lookup(var.per_node_data_disk_snapshot_max_retention_days, count.index + 1, var.data_disk_snapshot_max_retention_days)
      on_source_disk_delete = "APPLY_RETENTION_POLICY"
    }

    dynamic "snapshot_properties" {
      for_each = length(var.labels) > 0 ? [true] : []
      content {
        labels = var.labels
      }
    }
  }
}
